﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Fornecedor
{
    class FornecedorDatabase
    {

        public int Salvar(FornecedorDTO dto)
        {
            string script = @"insert into tb_fornecedor(nm_nome ,ds_cnpj,nm_telefone,ds_observacao,nm_email,ds_contato) 
                             Values(@nm_nome ,@ds_cnpj,@nm_telefone,@ds_observacao,@nm_email,@ds_contato)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", dto.Cnpj));
            parms.Add(new MySqlParameter("nm_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_observacao", dto.Observacao));
            parms.Add(new MySqlParameter("nm_email", dto.Email));
            parms.Add(new MySqlParameter("ds_contato", dto.Contato));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<FornecedorDTO> Listar()
        {
            string script = @"select * from tb_fornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id_fornecedor = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cnpj = reader.GetInt32("ds_cnpj");
                dto.Telefone = reader.GetInt32("nm_telefone");
                dto.Observacao = reader.GetString("ds_observacao");
                dto.Email = reader.GetString("nm_email");
                dto.Contato = reader.GetString("ds_contato");
                lista.Add(dto);

            }
            reader.Close();
            return lista;
        }
        public void Remover(int id_fornecedor)
        {
            string script = @"DELETE FROM id_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter(" id_fornecedor", id_fornecedor));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
