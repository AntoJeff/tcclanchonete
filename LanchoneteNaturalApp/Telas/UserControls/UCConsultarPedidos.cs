﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LanchoneteNaturalApp.BD.Classes.Pedido;

namespace LanchoneteNaturalApp.Telas.UserControls
{
    public partial class UCConsultarPedidos : UserControl
    {
        public UCConsultarPedidos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
   PedidoBusiness busi = new PedidoBusiness();
            List<PedidoDTO> Listar = busi.Listar() ;
            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = Listar;
            } 
            catch
            {
                MessageBox.Show("Error de busca!", "Consulta de Pedidos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
         

        }
    }
}
