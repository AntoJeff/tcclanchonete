﻿using LanchoneteNaturalApp.BD.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente(nm_nome,nm_endereco,nm_complemento,nr_cep,nr_telefone,nr_celular,ds_cpf,nm_sobrenome,dt_dia,dt_mes,dt_ano,ds_email) 
                              VALUES (@nm_nome,@nm_endereco,@nm_complemento,@nr_cep,@nr_telefone,@nr_celular,@ds_cpf,@nm_sobrenome,@dt_dia,@dt_mes,@dt_ano,@ds_email)"; 


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereco)); 
            parms.Add(new MySqlParameter("nm_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("nr_cep", dto.Cep));
            parms.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nr_celular", dto.Celular));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("dt_dia", dto.Dia));
            parms.Add(new MySqlParameter("dt_mes", dto.Mes));
            parms.Add(new MySqlParameter("dt_ano", dto.Ano));
            parms.Add(new MySqlParameter("ds_email", dto.Email));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<ClienteDTO> consultar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id_cliente = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.Endereco = reader.GetString("nm_endereco");
                dto.Complemento = reader.GetString("nm_complemento");
                dto.Cep = reader.GetInt32("nr_cep");
                dto.Telefone = reader.GetInt32("nr_telefone");
                dto.Celular = reader.GetInt32("nr_celular");
                dto.Cpf = reader.GetInt32("ds_cpf");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Dia = reader.GetInt32("dt_dia");
                dto.Mes = reader.GetInt32("dt_mes");
                dto.Ano = reader.GetInt32("dt_ano");
                dto.Email = reader.GetString("ds_email");
                lista.Add(dto);

            }
            reader.Close();

            return lista;
        } 

        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente
                                     SET nm_nome          = @nm_nome,
                                         nm_endereco      = @nm_endereco,
                                         nm_complemento   = @nm_complemento,
                                         nr_cep           = @nr_cep,
                                         nr_telefone      = @nr_telefone,
                                         nr_celular       = @nr_celular,
                                         ds_cpf           = @ds_cpf,
                                         nm_sobrenome     = @nm_sobrenome,
                                         dt_dia           = @dt_dia,
                                         dt_mes           = @dt_mes,
                                         dt_ano           = @dt_ano,
                                         ds_email         = @ds_email
                                       WHERE id_cliente   = @id_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("nm_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("nr_cep", dto.Cep));
            parms.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nr_celular", dto.Celular));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("dt_dia", dto.Dia));
            parms.Add(new MySqlParameter("dt_mes", dto.Mes));
            parms.Add(new MySqlParameter("dt_ano", dto.Ano));
            parms.Add(new MySqlParameter("ds_email", dto.Email));

            Database db = new Database();
           db.ExecuteInsertScriptWithPk(script, parms);


        }
        public void Remover(int id_cliente)
        {
            string script = @"DELETE FROM id_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id_cliente));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
