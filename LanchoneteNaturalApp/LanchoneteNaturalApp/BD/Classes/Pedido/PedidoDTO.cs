﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanchoneteNaturalApp.BD.Classes.Pedido
{
    class PedidoDTO
    {
        public int id_pedido { get; set; }
        public string produto { get; set; }
        public int quantidade { get; set; }
        public string  observacao { get; set; }
        public int telefone { get; set; }
        public int id_cardapio_pedido { get; set; }




    }
}
