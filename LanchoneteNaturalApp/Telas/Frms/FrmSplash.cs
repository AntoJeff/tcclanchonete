﻿using LanchoneteNaturalApp.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LanchoneteNaturalApp.Telas.Frms
{
    public partial class FrmSplash : Form
    {
        public FrmSplash()
        {
            InitializeComponent();
            // Inicia contagem para término da Splash Screen
            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    FrmLogin frm = new FrmLogin();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void FrmSplash_Load(object sender, EventArgs e)
        {

        }
    }
}
